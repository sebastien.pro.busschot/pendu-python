import os, json, re, random

def getScore(fileName):
    try:
        with open(fileName, "r") as fileScore:
            score = json.loads(fileScore.read())
    except OSError:
        print("Le fichier score n'existe pas. On va s'en occuper")
        with open(fileName, "w") as fileScore:
            score = []
    return score

def getWord():
    with open("dico.json") as words:
        listWords = json.loads(words.read())
        randomIndex = random.randint(0, len(listWords)-1)
        return listWords[randomIndex]

def getAlias(word):
    alias = ""
    for chars in word:
        alias += "_ "
    return alias

def getNewAlias(word, usedChars):
    alias = ""
    for char in word:
        if char in usedChars:
            alias += char+" "
        else:
            alias += "_ "
    return alias

def listToString(lst):
    string = ""
    for el in lst:
        string += str(el)
    return string

def getChar():
    while True:
        char = input("Choisissez une lettre : ")
        if(len(char) == 1):
            return char
        else:
            print("Merci de saisir UNE lettre")

def screen(usedChars, aliasWord, counter):
    print("\tWELCOME TO PENDU !!!\n\n{} || Stay tests : {} || You used chars : {}\n".format(aliasWord, counter, listToString(usedChars)))

def gameOver(aliasWord, counter):
    if(counter < 0 or "_" not in aliasWord):
        return True
    else:
        return False

def printHightScore(score):
    print("\nILS L'ONT AUSSI FAIT !!! Voici leurs résultats")
    for userScoreIndex in range(len(score)):
        for username, scores in score[userScoreIndex].items():
            print("\n{} :".format(username))
            for userScore in scores:
                for word, count in userScore.items():
                    if count < 0:
                        print("\t{}, pas trouvé".format(word))
                    else:
                        print("\t{}, trouvé avec {} essais restants".format(word, count))

def save(word, counter, username):
    fileName = "score.json"
    score = getScore(fileName)

    oldUser = False
    for userIndex in range(len(score)):
        if username in score[userIndex].keys():
            oldUser = True
            score[userIndex][username].append({word:counter})

    if not oldUser:
        score.append({username:[{word:counter}]})

    with open(fileName, "w") as fileScore:
            fileScore.write(json.dumps(score))

    printHightScore(score)