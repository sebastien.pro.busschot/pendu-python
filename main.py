#!/usr/bin/python3
 
# -*-coding:utf-8 -*

import os, math, gameEngine
# json.dumps({'4': 5, '6': 7}, sort_keys=True, indent=4)

while(True):
    username = input("\nNom du joueur ? ")
    word = gameEngine.getWord()
    usedChars = []
    aliasWord = gameEngine.getAlias(word)
    counter = math.ceil(len(word)*1.2)

    while(not gameEngine.gameOver(aliasWord, counter)):
        os.system('clear')
        gameEngine.screen(usedChars, aliasWord, counter)
        newChar = gameEngine.getChar()
        usedChars.append(newChar)
        aliasWord = gameEngine.getNewAlias(word,usedChars)
        counter -= 1

    gameEngine.screen(usedChars, aliasWord, counter)
    if counter < 0:
        print("\nC'étais {},On ne peut pas gagner à tous les coups ;)".format(word))
    else:
        print("Félicitations, nous tenons un vainqueur !")

    gameEngine.save(word, counter, username)

    if(input("\nQuitter? o/n : ") == "o"):
        break
